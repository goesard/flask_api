from sqlalchemy import create_engine, Column, String, Integer, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os
from dotenv import load_dotenv

load_dotenv()

Base = declarative_base()

db_host = os.environ.get('db_host')
db_user = os.environ.get('db_user')
db_password = os.environ.get('db_password')
db_port = os.environ.get('db_port')
db_name = os.environ.get('db_database')

# create_schema_stmt = DDL(f'CREATE SCHEMA {schema_name}')


class tbl_seqn(Base):
    __tablename__ = "appl_mst_seqn"
    __table_args__ = {'schema': 'orm'}

    code = Column("code", String, primary_key=True)
    prefix = Column("prefix", String)
    current_val = Column("current_val", Integer, autoincrement=True)
    created_date = Column("created_date", Date)
    created_by = Column("created_by", String)
    updated_date = Column("updated_date", Date)
    updated_by = Column("updated_by",String)

    def _init_(self, code, prefix, current_val, created_by, created_date, updated_by, updated_date):
        self.code = code
        self.prefix = prefix
        self.current_val = current_val
        self.created_by = created_by
        self.created_date = created_date
        self.updated_by = updated_by
        self.updated_date = updated_date

    def _repr_(self):
        return f"({self.code}) {self.prefix})"
    
DB_URL = f'postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}'
engine = create_engine(DB_URL)
Base.metadata.create_all(bind=engine)

Session = sessionmaker(bind=engine)
session = Session()

#person = Person(12354, "Mike", "Tyson", "Men")
#session.add(person)
session.commit()