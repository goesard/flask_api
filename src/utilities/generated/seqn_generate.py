import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.connections.connection import dbConnection
from .models.seqn_models import tbl_seqn

import os
from dotenv import load_dotenv

load_dotenv()

db_host = os.environ.get('db_host')
db_user = os.environ.get('db_user')
db_password = os.environ.get('db_password')
db_port = os.environ.get('db_port')
db_name = os.environ.get('db_database')

class generator(): 
    #def dbConnection():
    #    url = f'postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}'
    #    return url
    connection_db = dbConnection()
    engine = create_engine(connection_db)
    Session = sessionmaker(bind=engine)
    session = Session()

    
    def main(p_type):
        if p_type == 'USR':
            results = generator.generate_userid(p_type)
        elif p_type == 'TASK':    
            results = generator.generate_taskid(p_type)
        elif p_type == 'EMPL':    
            results = generator.gen_empl(p_type)
        return results

    # Gen Sequence UserID
    def generate_userid(p_type):

        get_seqn = generator.session.query(tbl_seqn.current_val).filter(tbl_seqn.code == p_type).first()
        p_prefix = 'USR'
        if get_seqn == None:
            new_seqn = tbl_seqn(code = p_type,
                                prefix = p_prefix,
                                current_val = 0,
                                created_date = datetime.datetime.now(),
                                created_by = 'System')
            generator.session.add(new_seqn)
            generator.session.commit()
        else:
            for row in get_seqn:
                data = row
            data = data + 1
            
            generator.session.query(tbl_seqn).filter(tbl_seqn.code == p_type).update({tbl_seqn.current_val: data})
            generator.session.commit()

        dt = datetime.datetime.now().strftime("%y%m%d")
        v_seqn = ('{:03d}'.format(data))
        return (str(dt + p_prefix + v_seqn))
    
    # Generate Sequence Task ID
    def generate_taskid(p_type):
        try:
            get_seqn = generator.session.query(tbl_seqn.current_val).filter(tbl_seqn.code == p_type).first()
            p_prefix = ''
            if get_seqn == None:
                new_seqn = tbl_seqn(code = p_type,
                                    prefix = p_prefix,
                                    current_val = 0,
                                    created_date = datetime.datetime.now(),
                                    created_by = 'System')
                generator.session.add(new_seqn)
                generator.session.commit()
            else:
                for row in get_seqn:
                    data = row
                data = data + 1
                
                generator.session.query(tbl_seqn).filter(tbl_seqn.code == p_type).update({tbl_seqn.current_val: data})
                generator.session.commit()

            dt = datetime.datetime.now().strftime("%y%m%d")
            v_seqn = ('{:04d}'.format(data))
            return (str(dt + p_prefix + str(v_seqn)))
        except Exception as e:
            print(e)
            return str(e)
        
    # Generate Employee ID
    def gen_empl(p_type):
        try:
            get_seqn = generator.session.query(tbl_seqn.current_val).filter(tbl_seqn.code == p_type).first()
            p_prefix = ""
            if get_seqn == None:
                new_seqn = tbl_seqn(code = p_type,
                                    prefix = '',
                                    current_val = 0,
                                    created_date = datetime.datetime.now(),
                                    created_by = 'System')
                generator.session.add(new_seqn)
                generator.session.commit()
            else:
                for row in get_seqn:
                    data = row
                data = data + 1
                
                generator.session.query(tbl_seqn).filter(tbl_seqn.code == p_type).update({tbl_seqn.current_val: data})
                generator.session.commit()

            dt = datetime.datetime.now().strftime("%y%m%d")
            v_seqn = ('{:04d}'.format(data))
            return (str(p_prefix + dt + str(v_seqn)))
        except Exception as e:
            print(e)
            return str(e)