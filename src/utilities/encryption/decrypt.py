import bcrypt


def decrypt(p_userPass, p_inputPass):

    # example password
    password = p_inputPass

# converting password to array of bytes
    bytes = password.encode('utf-8')

# Taking user entered password
    userPassword = p_userPass

# encoding user password
    userBytes = userPassword.encode('utf-8')

# checking password
    result = bcrypt.checkpw(bytes, userBytes)
    return result
