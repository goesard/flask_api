import bcrypt


def encrypt(p_password):
    try:
        bytes = p_password.encode('utf-8')
        salt = bcrypt.gensalt()
        hash = bcrypt.hashpw(bytes, salt)
        hash_dec = hash.decode('utf-8')
        return hash_dec
    except Exception as e:
        hash = print(str(e))
        return hash

encrypt('123')