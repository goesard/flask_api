# Import Dependencies
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from flask import make_response, jsonify
import datetime

# Import Modules
from src.connections.connection import dbConnection
from ..models.login_models import user_tables
from ....utilities.encryption.decrypt import decrypt


class loginController():

    # Define Variables
    connection_db = dbConnection()
    engine = create_engine(connection_db)
    Session = sessionmaker(bind=engine)
    session = Session()

    # Controllers
    def main(p_type, p_username, p_password):

        # Get all Users
        if p_type == 'SEE':
            data = loginController.get_data()
            return data

        # Login
        elif p_type == 'LOGIN':
            data = loginController.handleLogin(p_username, p_password)
            return data

    # Executor

    # Get all Users data
    def get_data():
        try:
            get_data = loginController.session.query(user_tables).all()
            data_list = []
            for row in get_data:
                data_dict = {
                    'username': row.username,
                    'password': row.password,
                    'date':row.created_date
                }
                data_list.append(data_dict)
            results = {'results': data_list}
            return results
        except Exception as e:
            print(str(e))
            results = {'err get data': str(e)}
            return results

    # Handle Login
    def handleLogin(p_username, p_password):
        try:
            # Master Authentication
            get_data = loginController.session.query(user_tables.username, user_tables.password).filter(
                user_tables.username == p_username).first()

            pass_auth = loginController.auth_password(p_username, p_password)

            # Condition if login does not match
            if ((get_data != None) and (pass_auth == True)):
                results = {'results': 'Login Successfully'}

                # Update Last Login
                loginController.session.query(user_tables).filter(user_tables.username == p_username).update({user_tables.last_login: datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")})
                loginController.session.commit()

                return make_response(jsonify(results), 200)
            
            else:
                results = {'results': 'Login Failed'}
                return make_response(jsonify(results), 401)

        except Exception as e:
            print(str(e))
            results = {'err login': str(e)}
            return results

    # Handle Auth Password
    def auth_password(p_username, p_password):
        try:
            get_pass = loginController.session.query(user_tables.password).filter(
                user_tables.username == p_username).first()

            for row in get_pass:
                data = row

            decrypt_pass = decrypt(p_userPass=data, p_inputPass=p_password)
            return decrypt_pass
        except Exception as e:
            print(str(e))
            return str(e)
