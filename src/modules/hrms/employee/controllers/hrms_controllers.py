# Import Dependencies
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from flask import make_response, jsonify
import datetime

# Import Modules
from .....connections.connection import dbConnection
from ..models.hrms_models import emp_tables
from .....utilities.generated.seqn_generate import generator

class hrmsControllers():
    connection_db = dbConnection()
    engine = create_engine(connection_db)
    Session = sessionmaker(bind=engine)
    session = Session()
    session.autoflush = True
    def main(p_type,
             p_firstname, 
             p_lastname, 
             p_gender,
             p_status, 
             p_department, 
             p_job,
             p_startdate,
             p_address,
             p_subdistrict,
             p_district,
             p_city,
             p_province,
             p_telp,
             created_by):
        
        if p_type == 'N':
            data = hrmsControllers.handleNewEmpl(p_firstname, 
                                                 p_lastname,
                                                 p_gender, 
                                                 p_status, 
                                                 p_department, 
                                                 p_job,
                                                 p_startdate,
                                                 p_address,
                                                 p_subdistrict,
                                                 p_district,
                                                 p_city,
                                                 p_province,
                                                 p_telp,
                                                 created_by)
        return data
    
    def handleNewEmpl(p_firstname, 
                      p_lastname, 
                      p_gender,
                      p_status, 
                      p_department, 
                      p_job,
                      p_startdate,
                      p_address,
                      p_subdistrict,
                      p_district,
                      p_city,
                      p_province,
                      p_telp,
                      p_created_by):
        try:
            seqn_emplid = generator.main("EMPL")
            new_empl = emp_tables(id = seqn_emplid,
                                  firstname = p_firstname,
                                  lastname = p_lastname,
                                  gender = p_gender,
                                  status = p_status,
                                  department = p_department,
                                  job = p_job,
                                  startdate = p_startdate,
                                  address = p_address,
                                  subdistrict = p_subdistrict,
                                  district = p_district,
                                  city = p_city,
                                  province = p_province,
                                  telp = p_telp,
                                  created_by = p_created_by,
                                  created_date = datetime.datetime.now())
            hrmsControllers.session.add(new_empl)
            hrmsControllers.session.commit()
            results = {'results': 'Register Success'}
            return make_response(jsonify(results), 200)
        except Exception as e:
            hrmsControllers.session.rollback()
            print(e)
            results = {'err register empl': str(e)}
            return make_response(jsonify(results), 501)
    
    