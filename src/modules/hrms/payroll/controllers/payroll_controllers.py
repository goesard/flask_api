from sqlalchemy import create_engine, Column, String, Integer, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os
from dotenv import load_dotenv

load_dotenv()
Base = declarative_base()

db_host = os.environ.get('db_host')
db_user = os.environ.get('db_user')
db_password = os.environ.get('db_password')
db_port = os.environ.get('db_port')
db_name = os.environ.get('db_database')

# Tables of Payroll
class emp_tables(Base):
    __table_args__ = {'schema':'orm'}
    __tablename__ = 'hr_payroll'

    id = Column("emp_id", String, primary_key=True)
    userid = Column("emp_userid", Integer, unique=True)
    firstname = Column("emp_firstname", String, nullable=False)
    lastname = Column("emp_lastname", String, nullable=False)
    gender = Column("emp_gender", String)
    status = Column("emp_status", String)
    department = Column("emp_department", String)
    job =  Column("emp_job", String)
    startdate = Column("emp_startdate", Date)
    address = Column("emp_address", String)
    subdistrict = Column("emp_subdistrict", String)
    district = Column("emp_district",String)
    city = Column("emp_city",String)
    province = Column("emp_province",String)
    telp = Column("emp_telp",String)
    created_by = Column("created_by",String)
    created_date = Column("created_date",Date)
    updated_by = Column("updated_by",String)
    updated_date = Column("updated_date",Date)

    def _init_(self, id, userid, firstname, lastname, gender, status, department, job, startdate, address, subdistrict, district, city, province, telp ,created_date, created_by, updated_by, updated_date):
        self.id = id
        self.userid = userid
        self.firstname = firstname
        self.lastname = lastname
        self.gender = gender
        self.status = status
        self.department = department
        self.job = job
        self.startdate = startdate
        self.address = address
        self.subdistrict = subdistrict
        self.district = district
        self.city = city
        self.province = province
        self.telp = telp
        self.created_by = created_by
        self.created_date = created_date
        self.updated_by = updated_by
        self.updated_date = updated_date
    
    def _repr_(self):
        return f"({self.id}) {self.userid})"

    
DB_URL = f'postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}'
engine = create_engine(DB_URL)
Base.metadata.create_all(bind=engine)

Session = sessionmaker(bind=engine)
session = Session()

#person = Person(12354, "Mike", "Tyson", "Men")
#session.add(person)
session.commit()