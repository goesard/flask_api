from sqlalchemy import Table, create_engine, Column, String, Integer, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os
from dotenv import load_dotenv

load_dotenv()
Base = declarative_base()

db_host = os.environ.get('db_host')
db_user = os.environ.get('db_user')
db_password = os.environ.get('db_password')
db_port = os.environ.get('db_port')
db_name = os.environ.get('db_database')

# Tables of Setup Cuti
class setup_cuti_tbl(Base):
    __table_args__ = {'schema':'orm'}
    __tablename__ = 'hr_setup_paidleave'

    id = Column("leave_id", String, primary_key=True)
    type = Column("leave_type", String)
    desc = Column("leave_desc", String)
    active = Column("leave_active", String)
    created_by = Column("created_by",String)
    created_date = Column("created_date",Date)
    updated_by = Column("updated_by",String)
    updated_date = Column("updated_date",Date)

    def _init_(self, id, type, desc, active, created_by, created_date, updated_by, updated_date):
        self.id = id
        self.type = type
        self.desc = desc
        self.active = active
        self.created_by = created_by
        self.created_date = created_date
        self.updated_by = updated_by
        self.updated_date = updated_date
    def _repr_(self):
        return f"({self.id}) {self.type})"

# Tables of Setup Tunjangan
class setup_allowance_tbl(Base):
    __table_args__ = {'schema':'orm'}
    __tablename__ = 'hr_setup_allowance'

    grade = Column("allowance_grade", String, primary_key=True)
    item = Column("allowance_item", String)
    min_plafon = Column("allowance_min_plafon", Integer)
    max_plafon = Column("allowance_max_plafon", Integer)
    active = Column("allowance_active", String)
    created_by = Column("created_by",String)
    created_date = Column("created_date",Date)
    updated_by = Column("updated_by",String)
    updated_date = Column("updated_date",Date)

    def _init_(self, grade, item, min_plafon, max_plafon, active, created_by, created_date, updated_by, updated_date):
        self.grade = grade
        self.item = item
        self.min_plafon = min_plafon
        self.max_plafon = max_plafon
        self.active =active
        self.created_by = created_by
        self.created_date = created_date
        self.updated_by = updated_by
        self.updated_date = updated_date
    def _repr_(self):
        return f"({self.grade}) {self.item})"
    
DB_URL = f'postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}'
engine = create_engine(DB_URL)
Base.metadata.create_all(bind=engine)

Session = sessionmaker(bind=engine)
session = Session()

#person = Person(12354, "Mike", "Tyson", "Men")
#session.add(person)
session.commit()