# Import Dependencies
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from flask import make_response, jsonify
import datetime

# Import Modules
from .....connections.connection import dbConnection
from ..models.hrms_setup_models import setup_cuti_tbl, setup_allowance_tbl

class hrmsSetupControllers():
    connection_db = dbConnection()
    engine = create_engine(connection_db)
    Session = sessionmaker(bind=engine)
    session = Session()
    session.autoflush = True
    def main(p_type,
             created_by,
             p_leave_id, 
             p_type_leave, 
             p_desc, 
             p_active,
             p_grade, 
             p_item, 
             p_min, 
             p_max):
        
        if p_type == 'SG':
            data = hrmsSetupControllers.handleSetupPaidLeave(p_leave_id, 
                                                             p_type_leave, 
                                                             p_desc, 
                                                             p_active, 
                                                             created_by)
        elif p_type == 'SA':
            data = hrmsSetupControllers.handleSetupAllowance(p_grade, 
                                                             p_item, 
                                                             p_min, 
                                                             p_max, 
                                                             p_active, 
                                                             created_by)
        return data
    
    # Setup Cuti
    def handleSetupPaidLeave(p_leave_id, 
                             p_type_leave, 
                             p_desc, 
                             p_active, 
                             p_created_by):
        try:
            newData = setup_cuti_tbl(id = p_leave_id, 
                                    type = p_type_leave, 
                                    desc = p_desc, 
                                    active = p_active, 
                                    created_by = p_created_by, 
                                    created_date = datetime.datetime.now())
            hrmsSetupControllers.session.add(newData)
            hrmsSetupControllers.session.commit()
            results = {'results': 'Setup Success'}
            return make_response(jsonify(results), 200)
        except Exception as e:
            hrmsSetupControllers.session.rollback()
            print(e)
            results = {'err setup cuti': str(e)}
            return make_response(jsonify(results), 501)
        
    # Setup Tunjangan
    def handleSetupAllowance(p_grade, p_item, p_min, p_max, p_active, p_created_by):
        try:
            allowance = setup_allowance_tbl(grade = p_grade,
                                            item = p_item,
                                            min_plafon = p_min,
                                            max_plafon = p_max,
                                            active = p_active,
                                            created_by = p_created_by, 
                                            created_date = datetime.datetime.now())
            hrmsSetupControllers.session.add(allowance)
            hrmsSetupControllers.session.commit()
            results = {'results': 'Setup Success'}
            return make_response(jsonify(results), 200)
        except Exception as e:
            hrmsSetupControllers.session.rollback()
            print(e)
            results = {'err setup cuti': str(e)}
            return make_response(jsonify(results), 501)
