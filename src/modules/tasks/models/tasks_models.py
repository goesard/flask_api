from sqlalchemy import create_engine, Column, String, Integer, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os
from dotenv import load_dotenv

load_dotenv()

Base = declarative_base()

db_host = os.environ.get('db_host')
db_user = os.environ.get('db_user')
db_password = os.environ.get('db_password')
db_port = os.environ.get('db_port')
db_name = os.environ.get('db_database')

class task_hdr_tbl(Base):
    __tablename__ = "appl_task_headers"
    __table_args__ = {'schema': 'orm'}

    task_id = Column("task_id", String, primary_key=True)
    task_name = Column("task_name", String)
    task_start = Column("task_start", Date)
    task_finish = Column("task_finish", Date)
    task_scope = Column("task_scope", String)
    task_project = Column("task_project",String)
    task_status = Column("task_status", String)
    created_by = Column("created_by",String)
    created_date = Column("created_date",Date)
    updated_by = Column("updated_by",String)
    updated_date = Column("updated_date",Date)

    def _init_(self, task_id, task_name, task_start, task_finish, task_scope, task_project, task_status, created_date, created_by, updated_by, updated_date):
        self.task_id = task_id
        self.task_name = task_name
        self.task_start = task_start
        self.task_finish = task_finish
        self.task_scope = task_scope
        self.task_project = task_project
        self.task_status = task_status
        self.created_date = created_date
        self.created_by = created_by
        self.updated_date = updated_date
        self.updated_by = updated_by

    def _repr_(self):
        return f"({self.task_id}) {self.task_name})"
    
class task_dtl_tbl(Base):
    __tablename__ = "appl_task_details"
    __table_args__ = {'schema': 'orm'}

    task_id = Column("task_id", String, primary_key=True)
    task_name = Column("task_name", String)
    task_desc = Column("task_desc", String)
    task_start = Column("task_start", Date)
    task_finish = Column("task_finish", Date)
    task_assign = Column("task_assign", String)
    task_owner = Column("task_owner",String)
    task_pic = Column("task_pic",String)
    created_by = Column("created_by", String)
    created_date = Column("created_date",Date)
    updated_by = Column("updated_by",String)
    updated_date = Column("updated_date",Date)

    def _init_(self, 
               task_id, 
               task_name, 
               task_desc, 
               task_start, 
               task_finish, 
               task_assign, 
               task_owner, 
               task_pic, 
               created_by,
               created_date, 
               updated_by, 
               updated_date):
        self.task_id = task_id
        self.task_name = task_name
        self.task_desc = task_desc
        self.task_start = task_start
        self.task_finish = task_finish
        self.task_assign = task_assign
        self.task_owner = task_owner
        self.task_pic = task_pic
        self.created_by = created_by
        self.created_date = created_date
        self.updated_date = updated_date
        self.updated_by = updated_by

    def _repr_(self):
        return f"({self.task_id}) {self.task_name} {self.task_desc})"


DB_URL = f'postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}'
engine = create_engine(DB_URL)
Base.metadata.create_all(bind=engine)

Session = sessionmaker(bind=engine)
session = Session()

#person = Person(12354, "Mike", "Tyson", "Men")
#session.add(person)
session.commit()