# Import Dependencies
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from flask import make_response, jsonify
from datetime import datetime
from sqlalchemy import column, select, join

# Import Modules
from ....connections.connection import dbConnection
from ..models.tasks_models import task_hdr_tbl, task_dtl_tbl
from ....utilities.generated.seqn_generate import generator

class taskController():

    # Define Variables
    connection_db = dbConnection()
    engine = create_engine(connection_db)
    Session = sessionmaker(bind=engine)
    session = Session()
    session.autoflush = True

    def main(p_type, 
             p_task_name,
             p_task_desc,
             p_task_scope,
             p_task_project,
             p_task_start,
             p_task_finish,
             p_task_assign,
             p_task_owner,
             p_task_pic,
             p_task_status,
             p_task_id,
             p_task_userid):

        # Input new Tasks
        if p_type == 'N':
            results = taskController.newTasks(p_task_name,
                                              p_task_desc,
                                              p_task_scope,
                                              p_task_project,
                                              p_task_start,
                                              p_task_finish,
                                              p_task_assign,
                                              p_task_owner,
                                              p_task_pic,
                                              p_task_status,
                                              p_task_userid)
        # Get new Tasks
        elif p_type == 'G':
            results = taskController.getTasks()

        # Edit Tasks
        elif p_type == 'E':
            results = taskController.editTasks(p_task_id, 
                                               p_task_start, 
                                               p_task_finish,
                                               p_task_status,
                                               p_task_userid)
        
        return results
        
    # Get All Tasks
    def getTasks():
        try:
            get_task = taskController.session.query(task_hdr_tbl, task_dtl_tbl).filter(task_hdr_tbl.task_id == task_dtl_tbl.task_id).all()
            data_list = []
            for row in get_task:
                data_dict = {
                    'task_id': row.task_hdr_tbl.task_id,
                    'task_name': row.task_hdr_tbl.task_name,
                    'task_start': row.task_hdr_tbl.task_start,
                    'task_finish': row.task_hdr_tbl.task_finish,
                    'task_scope': row.task_hdr_tbl.task_scope,
                    'task_status': row.task_hdr_tbl.task_status,
                    'task_desc': row.task_dtl_tbl.task_desc,
                    'task_assign': row.task_dtl_tbl.task_assign,
                    'task_owner': row.task_dtl_tbl.task_owner,
                    'task_pic': row.task_dtl_tbl.task_pic
                }
                data_list.append(data_dict)
            results = data_list
            return results
        except Exception as e:
            print(e)
            results = {'err controller': str(e)}
            return results

    # Get New Tasks
    def newTasks(p_task_name,
                 p_task_desc,
                 p_task_scope,
                 p_task_project,
                 p_task_start,
                 p_task_finish,
                 p_task_assign,
                 p_task_owner,
                 p_task_pic,
                 p_task_status,
                 p_task_userid):
        try:
            id = generator.main('TASK')

            # Task Headers
            hdr_tasks = task_hdr_tbl(task_id = id,
                                     task_name = p_task_name,
                                     task_scope = p_task_scope,
                                     task_project = p_task_project,
                                     task_start = p_task_start,
                                     task_finish = p_task_finish,
                                     created_by = p_task_userid,
                                     task_status = p_task_status,
                                     created_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            
            # Task Details
            dtl_tasks = task_dtl_tbl(task_id = id,
                                    task_name = p_task_name,
                                    task_desc = p_task_desc,
                                    task_start = p_task_start,
                                    task_finish = p_task_finish,
                                    task_assign = p_task_assign,
                                    task_owner = p_task_owner,
                                    task_pic = p_task_pic,
                                    created_by = p_task_userid,
                                    created_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                                    )

            taskController.session.add(hdr_tasks)
            taskController.session.add(dtl_tasks)
            taskController.session.commit()

            results = {'results': 'Generate Task Complete'}
            return make_response(jsonify(results), 200)
        except Exception as e:
            taskController.session.rollback()
            print(e)
            results = {'results': str(e)}
            return make_response(jsonify(results), 501)

    # Get Update Tasks
    def editTasks(p_task_id, 
                  p_task_start, 
                  p_task_finish,
                  p_task_status,
                  p_task_userid):
        try:

            # Update Headers
            taskController.session.query(task_hdr_tbl).filter(task_hdr_tbl.task_id == p_task_id).update({task_hdr_tbl.task_start : p_task_start,
                                                                                                         task_hdr_tbl.task_finish : p_task_finish,
                                                                                                         task_hdr_tbl.task_status : p_task_status,
                                                                                                         task_hdr_tbl.updated_by : p_task_userid,
                                                                                                         task_hdr_tbl.updated_date: datetime.now()})
            
            # Update Details
            taskController.session.query(task_dtl_tbl).filter(task_dtl_tbl.task_id == p_task_id).update({task_dtl_tbl.task_start : p_task_start,
                                                                                                         task_dtl_tbl.task_finish : p_task_finish,
                                                                                                         task_dtl_tbl.updated_by : p_task_userid,
                                                                                                         task_dtl_tbl.updated_date: datetime.now()})
            
            taskController.session.commit()
            results = {'results': 'Update Success'}
            return make_response(jsonify(results), 200)
        except Exception as e:
            taskController.session.rollback()
            results = {'err update': str(e)}
            return make_response(jsonify(results), 501)