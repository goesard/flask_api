# Import Dependencies
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from flask import make_response, jsonify
import datetime

# Import Modules
from ....connections.connection import dbConnection
from ..models.register_models import user_tables
from ....utilities.encryption.encrypt import encrypt
from ....utilities.generated.seqn_generate import generator


class registerControllers():
    # Define Variables
    connection_db = dbConnection()
    engine = create_engine(connection_db)
    Session = sessionmaker(bind=engine)
    session = Session()

    def main(p_type, 
             p_username,
             p_password, 
             p_role):
        
        if p_type == 'REGISTER':
            data = registerControllers.handleRegister(p_username, 
                                                      p_password, 
                                                      p_role)
            return data

    def handleRegister(p_username, 
                       p_password, 
                       p_role):
        try:
            seqn_user = generator.generate_userid(p_code = 'USERID')
            encrypt_pass = encrypt(p_password)
            new_usr = user_tables(id = seqn_user,
                                  username=p_username,
                                  password=encrypt_pass,
                                  role=p_role,
                                  is_active=True,
                                  last_login=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                  created_date=datetime.datetime.now(),
                                  created_by='System')
            
            registerControllers.session.add(new_usr)
            registerControllers.session.commit()

            results = {'results': 'Registration Complete'}
            return make_response(jsonify(results), 200)
        
        except Exception as e:
            print(str(e))
            results = {'err get data': str(e)}
            return make_response(jsonify(results), 501)
