from sqlalchemy import Column, String, Integer, Date, TIMESTAMP, Boolean
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class user_tables(Base):
    __table_args__ = {'schema':'orm'}
    __tablename__ = 'appl_users'

    id = Column("id", Integer, primary_key=True, autoincrement='auto')
    username = Column("username", String, nullable=False, unique=True)
    password = Column("password", String, nullable=False)
    role = Column("role", String)
    is_active = Column("is_active", Boolean)
    last_login =  Column("last_login", TIMESTAMP)
    created_date = Column("created_date", Date)
    created_by = Column("created_by", String)
    updated_date = Column("updated_date", Date)
    updated_by = Column("updated_by",String)