import os
from dotenv import load_dotenv

load_dotenv()

db_host = os.environ.get('db_host')
db_user = os.environ.get('db_user')
db_password = os.environ.get('db_password')
db_port = os.environ.get('db_port')
db_name = os.environ.get('db_database')

def dbConnection():
    url = f'postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}'
    return url