from sqlalchemy import create_engine, ForeignKey, Column, String, Integer, CHAR, Date, TIMESTAMP, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os
from dotenv import load_dotenv

load_dotenv()

Base = declarative_base()

db_host = os.environ.get('db_host')
db_user = os.environ.get('db_user')
db_password = os.environ.get('db_password')
db_port = os.environ.get('db_port')
db_name = os.environ.get('db_database')

# create_schema_stmt = DDL(f'CREATE SCHEMA {schema_name}')


class Person(Base):
    __tablename__ = "appl_users"
    __table_args__ = {'schema': 'orm'}

    id = Column("id", String, primary_key=True)
    username = Column("username", String, nullable=False, unique=True)
    password = Column("password", String, nullable=False)
    role = Column("role", String)
    is_active = Column("is_active", Boolean)
    last_login =  Column("last_login", TIMESTAMP)
    created_date = Column("created_date", Date)
    created_by = Column("created_by", String)
    updated_date = Column("updated_date", Date)
    updated_by = Column("updated_by",String)

    def _init_(self, id, username, password, role, is_active, last_login, created_date, created_by, updated_date, updated_by):
        self.id = id
        self.username = username
        self.password = password
        self.role = role
        self.is_active = is_active
        self.last_login = last_login
        self.created_date = created_date
        self.created_by = created_by
        self.updated_date = updated_date
        self.updated_by = updated_by

    def _repr_(self):
        return f"({self.id}) {self.username} {self.password} ({self.role})"


DB_URL = f'postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}'
engine = create_engine(DB_URL, echo=True)
Base.metadata.create_all(bind=engine)

Session = sessionmaker(bind=engine)
session = Session()

#person = Person(12354, "Mike", "Tyson", "Men")
#session.add(person)
session.commit()