# Import Dependencies
from flask import Flask, request, make_response, jsonify, render_template
from flask_cors import CORS


# Import Modules
############################## Import Modules Login #################################
from src.modules.login.controllers.login_controllers import loginController

############################## Import Modules Register #################################
from src.modules.registers.controllers.register_controllers import registerControllers

############################## Import Modules Task #################################
from src.modules.tasks.controllers.tasks_controllers import taskController

############################## Import Modules HRMS Setup #################################
# Register New Employee 
from src.modules.hrms.employee.controllers.hrms_controllers import hrmsControllers
# Setup Cuti
from src.modules.hrms.setup.controllers.hrms_setup_controllers import hrmsSetupControllers

app = Flask(__name__)
CORS(app)


@app.route('/v1/test', methods=['GET'])
def test_get():
    try:
        results = {'results': 'test get'}
        return make_response(jsonify(results), 200)
    except Exception as e:
        results = {'results': str(e)}
        return make_response(jsonify(results), 500)


@app.route('/v1/users/all', methods=['GET'])
def get_users():
    try:
        results = loginController.main(p_type='SEE',
                                       p_username='',
                                       p_password='')
        return make_response(jsonify(results), 200)
    except Exception as e:
        print('error get users', str(e))
        results = {'results': str(e)}
        return make_response(jsonify(results), 500)

################################# Modules Login #################################

# Login Auth
@app.route('/v1/login', methods=['POST'])
def login():
    params = request.get_json()
    results = loginController.main(p_type='LOGIN',
                                   p_username=params['username'],
                                   p_password=params['password'])
    return results

################################# Modules Register #################################

@app.route('/v1/register', methods=['POST'])
def register():
    params = request.get_json()
    results = registerControllers.main(p_type='REGISTER',
                                       p_username = params['username'], 
                                       p_password = params['password'], 
                                       p_role = params['role']
                                       )
    return results

################################# Modules Tasks #################################

# Test Server Side Rendering
@app.route('/v1/task/test', methods = ['GET'])
def test():
    data = taskController.main(p_type = 'G',
                                  p_task_name = '',
                                  p_task_desc = '',
                                  p_task_scope = '',
                                  p_task_project = '',
                                  p_task_start = '',
                                  p_task_finish = '',
                                  p_task_assign = '',
                                  p_task_owner = '',
                                  p_task_pic = '',
                                  p_task_status = '',
                                  p_task_id = '',
                                  p_task_userid= '')
    results = render_template('index.html', data = data)
    return results


#Get Tasks Headers
@app.route('/v1/task/all', methods= ['GET'])
def getTasks():
    results = taskController.main(p_type = 'G',
                                  p_task_name = '',
                                  p_task_desc = '',
                                  p_task_scope = '',
                                  p_task_project = '',
                                  p_task_start = '',
                                  p_task_finish = '',
                                  p_task_assign = '',
                                  p_task_owner = '',
                                  p_task_pic = '',
                                  p_task_status = '',
                                  p_task_id = '',
                                  p_task_userid= '')
    return results


# Create New Tasks
@app.route('/v1/task/new', methods=['POST'])
def newTasks():
    params = request.get_json()
    results = taskController.main(p_type = 'N',
                                  p_task_name = params['task_name'],
                                  p_task_desc = params['task_desc'],
                                  p_task_scope = params['task_scope'],
                                  p_task_project = params['task_project'],
                                  p_task_start = params['task_start'],
                                  p_task_finish = params['task_finish'],
                                  p_task_assign = params['task_assign'],
                                  p_task_owner = params['task_owner'],
                                  p_task_pic = params['task_pic'],
                                  p_task_status = params['task_status'],
                                  p_task_id = '',
                                  p_task_userid= params['task_userid'])
    return results

# Update Tasks
@app.route('/v1/task/update', methods=['POST'])
def updateTasks():
    params = request.get_json()
    results = taskController.main(p_type = 'E',
                                  p_task_name = '',
                                  p_task_desc = '',
                                  p_task_scope = '',
                                  p_task_project = '',
                                  p_task_start = params['task_start'],
                                  p_task_finish = params['task_finish'],
                                  p_task_assign = '',
                                  p_task_owner = '',
                                  p_task_pic = '',
                                  p_task_status = params['task_status'],
                                  p_task_id = params['task_id'],
                                  p_task_userid= params['task_userid'])
    return results

################################# Modules HRMS #################################

# Register New Employee
@app.route('/v1/hrms/new', methods=['POST'])
def newEmp():
    params = request.get_json()
    results = hrmsControllers.main(p_type = 'N',
                                   p_firstname = params['firstname'], 
                                   p_lastname= params['lastname'],
                                   p_gender = params['gender'],
                                   p_status = params['status'], 
                                   p_department = params['department'], 
                                   p_job = params['job'],
                                   p_startdate = params['startdate'],
                                   p_address = params['address'],
                                   p_subdistrict = params['subdistrict'],
                                   p_district = params['district'],
                                   p_city = params['city'],
                                   p_province = params['province'],
                                   p_telp = params['telp'],
                                   created_by = params['created_by'])
    return results

# Setup Cuti
@app.route('/v1/hrms/setup/paidleave', methods=['POST'])
def setupPaidleave():
    params = request.get_json()
    results = hrmsSetupControllers.main(p_type = 'SG',
                                   created_by = params['created_by'],
                                   p_leave_id = params['leave_id'], 
                                   p_type_leave = params['leave_type'], 
                                   p_desc = params['desc'], 
                                   p_active = params['active'],
                                   p_grade = '', 
                                   p_item = '', 
                                   p_min = '',  
                                   p_max = '')
    return results

# Setup Tunjangan
@app.route('/v1/hrms/setup/allowance', methods=['POST'])
def setupAllowance():
    params = request.get_json()
    results = hrmsSetupControllers.main(p_type = 'SA',
                                        created_by = params['created_by'],
                                        p_leave_id = '', 
                                        p_type_leave = '', 
                                        p_desc = '', 
                                        p_active = params['active'],
                                        p_grade = params['grade'], 
                                        p_item = params['item'], 
                                        p_min = params['min'],  
                                        p_max = params['max'])
    return results

if __name__ == '__main__':
    app.run(debug=True)
